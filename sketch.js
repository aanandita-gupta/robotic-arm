var fSizes = [[50,160,50], 
              [50,200,50], 
              [50,240,50], 
              [50,200,50], 
              [50,140,50]];
var palmSize = [240,200,50];
var armSize = [170,300,50];
var fingerSpacing = 63;

function setup() 
{
    createCanvas(windowWidth, windowHeight, WEBGL);
}

function draw() 
{
//    rotateY(radians(frameCount));
    push();
        rotateY(sin(frameCount/100));
        rotateZ(sin(frameCount/100));
        background(125);
        /*translating right in the beginning so that the whole hand fits on the screen (assuming the arm starts from origin) */
        translate(0,210);
        box(palmSize[0] * 2/3, 275, 30);

        push();
            //making it start from where the arm ends
            translate(0,-150);
            palm(
                palmSize[0],
                palmSize[1], 
                palmSize[2], 
                (sin(radians(frameCount)) - 1)/2
            );
        pop();
    pop();
    
//    for(var i = 0; i < fSizes.length; i++)
//    {
//        translate(palmSize[0], palmSize[1]);
//        finger(fSizes[i][0], 
//               fSizes[i][1], 
//               fSizes[i][2],
//               0);
//               //(sin(radians(frameCount)) - 1)/2);
//    }
}

/*takes four parameters:
    - w: width of the phalanx
    - h: height of the phalanx
    - d: depth of the phalanx
    - bend: indicates at which stage of the bending that part of the finger is
*/
function phalanx(w, h, d, bend)
{
    var maxAngle = 60;
    //bend = constrain(bend, 0, 1);
    //push();
    
    /*Making sure that when the bend is 0 then the phalanx is straight up*/ 
    rotateX(radians(maxAngle * bend));
    //translate(200,200);
    translate(0, -h/2);
    sphere(w*0.7, h, d);
    //pop();
}

function palm(w, h, d, bend)
{
    var maxAngle = 10;
    
    //making the palm forwards and backwards
    rotateX(radians(maxAngle * (-bend * 2)));
    translate(0, -h/2);
    //creating the palm - 3D box 
    box(w, h, d);

    for(var i = 1; i < fSizes.length; i++)
    {
        /* Move in the right location using translate() and place one finger at the top, left corner of the palm 
        This is only for 4 fingers - index, middle, ring and little finger
        Using push and pop to make sure that each finger is drawn at the correct position before translating it again.
        */
        push();
            /*Use the variable fingerSpacing to know how far each finger will be from the next.*/
            translate((-w * 0.655) + (i * fingerSpacing), -h/2 - 5);
            finger(
                fSizes[i][0],
                fSizes[i][1],
                fSizes[i][2],
                (sin(radians(frameCount * 2 + (360/11) * i)) + 1)/2
                /*Giving each finger a slightly different phase so that they open and close in order */
            );
        pop();
    }

    /*Making the thumb at a different angle than the rest of the fingers and then rotating it on the Z- axis */
    push ();
        translate(-w/2 - 10,h * 0.1);
        rotateZ(radians(-270));
        finger(
            fSizes[0][0],
            fSizes[0][1],
            fSizes[0][2],
            (sin(radians(frameCount * 2 + (360/10))) + 1)/2
        );
    pop();
    
    /*Making the face on the palm*/
    push();
        //NOSE
        push();
            translate(0, 0, 10);
            sphere(20);
        pop();
        
        //LEFT EYE
        push();
            translate(-40,-40,10);
            sphere(40);
        pop();
    
        //RIGHT EYE
        push();
            translate(40,-40,10);
            sphere(40);
        pop();
    
        //SMILE
        push();
            for(var i = 60; i >= -60; i = i-20)
            {
                push();
                    translate(i,30, 10);
                    sphere(20);
                pop();
                push();
                    if(i == 0)
                    {
                        translate(i,75,10);
                        sphere(20);
                    }
                    else if(i == 20 || i == -20)
                    {
                        translate(i,70,10);
                        sphere(20);
                    }
                    else if(i == 40 || i == -40)
                    {
                        translate(i,55,10);
                        sphere(20);
                    }
                pop();
            }
        pop();
    pop();
}

function finger(w, h, d, bend)
{
    cylinder(5,30);
    //passing only 40% of the finger height
    phalanx(w, h * 0.4, d, bend);
    
    translate(0, -h * 0.2);
    //sphere(10);
    cylinder(5,30);
    //passing only 40% of the finger height
    phalanx(w, h * 0.4, d, bend);
    
    translate(0, -h * 0.2);
    //sphere(10);
    cylinder(5,30);
    //passing only 20% of the finger height
    phalanx(w, h * 0.2, d, bend)
}

